class AddRaspberryidToAssignments < ActiveRecord::Migration
  def change
    add_reference :assignments, :raspberries, index: true
  end
end
