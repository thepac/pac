class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.datetime :time
      t.integer :unit_id

      t.timestamps
    end
  end
end
