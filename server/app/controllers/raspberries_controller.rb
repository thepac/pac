class RaspberriesController < ApplicationController
  before_action :set_raspberry, only: [:show, :edit, :update, :destroy]

  # GET /raspberries
  # GET /raspberries.json
  def index
    if check_password?(params[:password])
      unit_id = params[:notification][:unit_id]
      time = params[:notification][:time]
      user = Assignment.where(raspberries_id: unit_id)
      user = user.first
      the_user = User.find(user.user_id)
      PacMailer.notification_email(the_user).deliver
      @note = Notification.new
      @note.time = time
      @note.unit_id = unit_id
      @note.save
     redirect_to notifications_path   
    end    
  end

  def check_password?(password)
    flag = false
    flag = true if password == '12345'
    flag
  end

  # GET /raspberries/1
  # GET /raspberries/1.json
  def show
  end

  # GET /raspberries/new
  def new
    @raspberry = Raspberry.new
  end

  # GET /raspberries/1/edit
  def edit
  end

  # POST /raspberries
  # POST /raspberries.json
  def create
    @raspberry = Raspberry.new
    @raspberry.unit_id = raspberry_params[:unit_id].to_i

    respond_to do |format|
      if @raspberry.save
        format.html { redirect_to root_path, notice: 'Raspberry was successfully created.' }
        format.json { render :show, status: :created, location: @raspberry }
      else
        format.html { render :new }
        format.json { render json: @raspberry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /raspberries/1
  # PATCH/PUT /raspberries/1.json
  def update
    respond_to do |format|
      if @raspberry.update(raspberry_params)
        format.html { redirect_to @raspberry, notice: 'Raspberry was successfully updated.' }
        format.json { render :show, status: :ok, location: @raspberry }
      else
        format.html { render :edit }
        format.json { render json: @raspberry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /raspberries/1
  # DELETE /raspberries/1.json
  def destroy
    @raspberry.destroy
    respond_to do |format|
      format.html { redirect_to raspberries_url, notice: 'Raspberry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_raspberry
      @raspberry = Raspberry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def raspberry_params
      params.require(:raspberry).permit(:unit_id)
    end
end
