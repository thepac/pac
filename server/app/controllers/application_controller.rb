class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def build_json_array
    begin      
      @notes = Notification.all
      @notes = Notification.sort(@notes)
      notifications = { json: @notes}
        respond_to do |format|
          format.html
          format.json { render notifications } 
        end
    rescue
      redirect_to root_path
    end 
  end

end
