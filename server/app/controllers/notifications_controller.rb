class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]

  # GET /notifications
  # GET /notifications.json
  def index
    if current_user.try(:admin)
      @users = User.where(admin: false)
      flag = params[:flag]
      user_id = params[:user_id]
      if flag == "delete"
        user = User.find(user_id)
        user.destroy
        redirect_to notifications_path
      end
    elsif user_signed_in?
      @notifications = Notification.all
      @notifications = Notification.sort(@notifications)
      assignments = Assignment.where(user_id: current_user.id)
      assign = Array.new
      assignments.each {|a| assign.push(a.raspberries_id)}
      @notifications = @notifications.to_a.select {|n| assign.include?(n.unit_id)} 
    end
  end

  def add_raspberry
    if params[:flag] == "assign"
      assign = Assignment.new
      assign.user_id = params[:user_id]
      assign.raspberries_id = params[:unit_id]
      assign.save
      redirect_to add_raspberry_path(user_id: params[:user_id])
    elsif params[:flag] == "remove"
      assignments = Assignment.where(raspberries_id: params[:unit_id].to_i)
      assignments.each {|a| a.destroy }
      redirect_to add_raspberry_path(user_id: params[:user_id])
    end
    
    @user = User.find(params[:user_id])
    assignments = Assignment.where(user_id: params[:user_id])
    user_unit_ids = Array.new
    assignments.each {|a| user_unit_ids.push(a.raspberries_id)}
    @unit_ids = Raspberry.all
    @unit_ids_left = @unit_ids.to_a.select {|u| user_unit_ids.include?(u.unit_id) == false}
    @unit_ids_have = @unit_ids.to_a.select {|u| user_unit_ids.include?(u.unit_id) == true}
  end

  # GET /notifications/1
  # GET /notifications/1.json
  def show
  end

  # GET /notifications/new
  def new
    @notification = Notification.new(notification_params)
  end

  # GET /notifications/1/edit
  def edit
  end

  # POST /notifications
  # POST /notifications.json
  def create

    @notification = Notification.new(notification_params)

    respond_to do |format|
      if @notification.save
        format.html { redirect_to @notification, notice: 'Notification was successfully created.' }
        format.json { render :show, status: :created, location: @notification }
      else
        format.html { render :new }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notifications/1
  # PATCH/PUT /notifications/1.json
  def update
    respond_to do |format|
      if @notification.update(notification_params)
        format.html { redirect_to @notification, notice: 'Notification was successfully updated.' }
        format.json { render :show, status: :ok, location: @notification }
      else
        format.html { render :edit }
        format.json { render json: @notification.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notifications/1
  # DELETE /notifications/1.json
  def destroy
    @notification.destroy
    respond_to do |format|
      format.html { redirect_to notifications_url, notice: 'Notification was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:time, :unit_id,:user_id)
    end
end
