class Notification < ActiveRecord::Base

  def self.sort(notifications)
    notifications.sort_by(&:created_at)
    notifications = notifications.to_a.reverse
  end

end
