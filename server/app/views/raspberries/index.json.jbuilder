json.array!(@raspberries) do |raspberry|
  json.extract! raspberry, :id
  json.url raspberry_url(raspberry, format: :json)
end
