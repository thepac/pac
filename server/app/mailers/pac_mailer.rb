class PacMailer < ActionMailer::Base
  default from: "PACSecurity@gmail.com"
  def notification_email(user)
    @user = user
    mail(to: @user.email, subject: "PACSecurity Alert")
  end
end
