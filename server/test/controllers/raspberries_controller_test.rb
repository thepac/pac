require 'test_helper'

class RaspberriesControllerTest < ActionController::TestCase
  setup do
    @raspberry = raspberries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:raspberries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create raspberry" do
    assert_difference('Raspberry.count') do
      post :create, raspberry: {  }
    end

    assert_redirected_to raspberry_path(assigns(:raspberry))
  end

  test "should show raspberry" do
    get :show, id: @raspberry
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @raspberry
    assert_response :success
  end

  test "should update raspberry" do
    patch :update, id: @raspberry, raspberry: {  }
    assert_redirected_to raspberry_path(assigns(:raspberry))
  end

  test "should destroy raspberry" do
    assert_difference('Raspberry.count', -1) do
      delete :destroy, id: @raspberry
    end

    assert_redirected_to raspberries_path
  end
end
