package com.example.paul.pacsecurity;

/**
 * Created by Paul on 7/23/2015.
 */
public final class Settings {
    public final static int UPDATE_INTERVAL_SECONDS = 5;
    public final static String JSON_URL = "http://the-pac.herokuapp.com/build_json_array.json";
}
