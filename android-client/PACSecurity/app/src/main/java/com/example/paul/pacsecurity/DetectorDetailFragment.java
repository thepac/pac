package com.example.paul.pacsecurity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * A fragment representing a single Detector detail screen.
 * This fragment is either contained in a {@link DetectorListActivity}
 * in two-pane mode (on tablets) or a {@link DetectorDetailActivity}
 * on handsets.
 */
public class DetectorDetailFragment extends Fragment {
    private final static String tag = "DetectorDetailFragment";
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private NotificationManager.RaspberryPi mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public DetectorDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = NotificationManager.INSTANCE.getPiList().get(Integer.parseInt(getArguments().getString(ARG_ITEM_ID)));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detector_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            this.view = ((ListView) rootView.findViewById(R.id.detector_detail));
            this.updateList();
        }

        scheduledPool.scheduleWithFixedDelay(updatePisRunnable, Settings.UPDATE_INTERVAL_SECONDS,Settings.UPDATE_INTERVAL_SECONDS, TimeUnit.SECONDS);

        return rootView;
    }

    private ListView view = null;

    private void updateList() {
        if (view != null)
        {
            view.setAdapter(
                    new ArrayAdapter<NotificationManager.Notification>(
                            getActivity(),
                            android.R.layout.simple_list_item_activated_1,
                            android.R.id.text1,
                            mItem.getNotifications()));
        }
    }

    private class JsonCallTask extends AsyncTask<Void, Void, String> {

        @Override protected String doInBackground(Void... a){
            return StaticMethods.retrieveJsonString();
        }

        @Override protected void onPostExecute(String jsonString) {
            try {
                NotificationManager.INSTANCE.update(jsonString);
                NotificationManager.RaspberryPi pi = NotificationManager.INSTANCE.getPi(mItem.getId());
                mItem = pi == null ? mItem : pi;
                updateList();
            }catch (Exception e)
            {
                Log.e(tag,"Failed to update notificationmanager");
            }
        }
    }

    ScheduledExecutorService scheduledPool = Executors.newScheduledThreadPool(4);

    @Override public void onStop() {
        super.onStop();
        this.scheduledPool.shutdown();
    }

    Runnable updatePisRunnable = new Runnable()
    {
        @Override
        public void run()
        {
            new JsonCallTask().execute();
        }
    };
}
