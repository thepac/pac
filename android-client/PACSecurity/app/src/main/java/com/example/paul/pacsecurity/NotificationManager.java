package com.example.paul.pacsecurity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Paul on 7/21/2015.
 */
public enum NotificationManager{
    INSTANCE;

    public void update(String jsonString) throws JSONException, ParseException {
        map = new HashMap<Integer,RaspberryPi>();

        JSONArray jsonArray = new JSONArray(jsonString);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int notificationId = jsonObject.getInt("id");
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(jsonObject.getString("time"));
            int piId = jsonObject.getInt("unit_id");

            if (! map.containsKey(piId))
                map.put(piId, new RaspberryPi(piId, new ArrayList<Notification>()));

            map.get(piId).addNotification(new Notification(notificationId, date));
        }
    }

    public List<RaspberryPi> getPiList() {
        assert map != null;

        List<RaspberryPi> result = new ArrayList<RaspberryPi>();
        for (RaspberryPi pi : this.map.values())
            result.add(pi);
        return result;
    }

    public RaspberryPi getPi(int id){
        return this.map.get(id);
    }

    private Map<Integer,RaspberryPi> map;

    public final static class RaspberryPi {
        public int getId() {
            return this.id;
        }

        public List<Notification> getNotifications() {
            return Collections.unmodifiableList(this.notifications);
        }

        @Override public String toString() {
            return "Raspberry Pi " + this.id;
        }

        private final int id;
        private final List<Notification> notifications;

        private RaspberryPi(int id, List<Notification> notifications){
            this.id = id;
            this.notifications = notifications;

            assert this.notifications != null;
        }

        private void addNotification(Notification notification)
        {
            this.notifications.add(notification);
        }
    }

    public final static class Notification {
        public int getId(){
            return this.id;
        }

        public Date getDate() {
            return new Date(this.date.getTime());
        }

        @Override public String toString(){
            return this.date.toString();
        }

        private final int id;
        private final Date date;

        private Notification(int id, Date date)
        {
            this.id = id;
            this.date = date;

            assert this.date != null;
        }
    }
}
