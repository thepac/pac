package com.example.paul.pacsecurity;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Paul on 7/23/2015.
 */
public final class StaticMethods {
    private StaticMethods(){}
    private final static String tag = "StaticMethods";

    public static String retrieveJsonString() {
        String jsonString = null;

        URLConnection urlConnection = null;
        try {
            URL url = new URL(Settings.JSON_URL);
            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());


            java.util.Scanner s = new java.util.Scanner(in).useDelimiter("\\A");
            jsonString =  s.hasNext() ? s.next() : "";

            in.close();
        }
        catch (Exception e) {
            Log.e(tag,"Failed to retrieve json",e);
        }
        return jsonString;
    }
}
