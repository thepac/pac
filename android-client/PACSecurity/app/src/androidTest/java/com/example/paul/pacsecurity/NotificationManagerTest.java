package com.example.paul.pacsecurity;

import android.test.AndroidTestCase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul on 7/21/2015.
 */
public class NotificationManagerTest extends AndroidTestCase {

    public void testAdd() throws Exception{
        NotificationManager.INSTANCE.update("[{\"id\":70,\"time\":\"2015-06-23T16:04:07.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:04:07.412Z\",\"updated_at\":\"2015-06-23T20:04:07.412Z\"},{\"id\":69,\"time\":\"2015-06-23T16:03:36.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:03:37.036Z\",\"updated_at\":\"2015-06-23T20:03:37.036Z\"},{\"id\":68,\"time\":\"2015-06-23T16:03:26.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:03:27.033Z\",\"updated_at\":\"2015-06-23T20:03:27.033Z\"},{\"id\":67,\"time\":\"2015-06-23T16:03:07.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:03:07.612Z\",\"updated_at\":\"2015-06-23T20:03:07.612Z\"},{\"id\":66,\"time\":\"2015-06-23T16:03:01.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:03:01.852Z\",\"updated_at\":\"2015-06-23T20:03:01.852Z\"},{\"id\":65,\"time\":\"2015-06-23T16:02:47.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:02:48.125Z\",\"updated_at\":\"2015-06-23T20:02:48.125Z\"},{\"id\":64,\"time\":\"2015-06-23T16:02:32.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:02:32.663Z\",\"updated_at\":\"2015-06-23T20:02:32.663Z\"},{\"id\":63,\"time\":\"2015-06-23T16:02:26.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T20:02:26.956Z\",\"updated_at\":\"2015-06-23T20:02:26.956Z\"},{\"id\":62,\"time\":\"2015-06-23T18:40:48.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T18:40:49.365Z\",\"updated_at\":\"2015-06-23T18:40:49.365Z\"},{\"id\":61,\"time\":\"2015-06-23T18:40:47.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T18:40:48.533Z\",\"updated_at\":\"2015-06-23T18:40:48.533Z\"},{\"id\":60,\"time\":\"2015-06-23T18:40:43.000Z\",\"unit_id\":1,\"created_at\":\"2015-06-23T18:40:47.605Z\",\"updated_at\":\"2015-06-23T18:40:47.605Z\"}]");

        Map<Integer,NotificationManager.RaspberryPi> map = new HashMap<Integer,NotificationManager.RaspberryPi>();
        for (NotificationManager.RaspberryPi pi : NotificationManager.INSTANCE.getPiList())
            map.put(pi.getId(),pi);

        assertEquals(1, map.size());
        assertTrue(map.containsKey(1));
        NotificationManager.RaspberryPi pi = map.get(1);
        assertEquals(11, pi.getNotifications().size());

        int count = 0;
        NotificationManager.Notification notification = null;
        for (NotificationManager.Notification not : pi.getNotifications())
            if (not.getId() == 68) {
                count++;
                notification = not;
            }
        assertEquals(1, count);

        assertEquals(2015 - 1900, notification.getDate().getYear());
        assertEquals(6 - 1, notification.getDate().getMonth());
        assertEquals(23,notification.getDate().getDate());
        assertEquals(16,notification.getDate().getHours());
        assertEquals(3,notification.getDate().getMinutes());
        assertEquals(26,notification.getDate().getSeconds());
        assertEquals(0,notification.getDate().getTime() % 1000);
    }
}
