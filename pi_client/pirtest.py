import RPi.GPIO as GPIO
import time
import datetime
import requests

def get_time():
    return datetime.datetime.now().strftime("%Y-%m-%d_%H.%M.%S")

sensor = 4

GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor, GPIO.IN, GPIO.PUD_DOWN)
previous_state = False
current_state = False

while True:
    time.sleep(0.1)
    previous_state = current_state
    current_state = GPIO.input(sensor)
    if (current_state != previous_state) and current_state:
        r = requests.get('http://the-pac.herokuapp.com/raspberries?notification[time]='+get_time()+'&notification[unit_id]=1&password=12345')
